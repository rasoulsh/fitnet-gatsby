import { createGlobalStyle } from 'styled-components';

const Typography = createGlobalStyle`



@font-face {
    font-family: Dana;
    font-style: normal;
    font-weight: 300;
    src: url('../assets/fonts/eot/dana-light.eot');
    src: url('../assets/fonts/eot/dana-light.eot?#iefix') format('embedded-opentype'),  /* IE6-8 */
      url('../assets/fonts/woff2/dana-light.woff2') format('woff2'),  /* FF39+,Chrome36+, Opera24+*/
      url('../assets/fonts/woff/dana-light.woff') format('woff');  /* FF3.6+, IE9, Chrome6+, Saf5.1+*/
}

@font-face {
    font-family: Dana;
    font-style: normal;
    font-weight: 400;
    src: url('../assets/fonts/eot/dana-medium.eot');
    src: url('../assets/fonts/eot/dana-medium.eot?#iefix') format('embedded-opentype'),  /* IE6-8 */
      url('../assets/fonts/woff2/dana-medium.woff2') format('woff2'),  /* FF39+,Chrome36+, Opera24+*/
      url('../assets/fonts/woff/dana-medium.woff') format('woff');  /* FF3.6+, IE9, Chrome6+, Saf5.1+*/
}
@font-face {
    font-family: Dana;
    font-style: normal;
    font-weight: 500;
    src: url('../assets/fonts/eot/dana-demibold.eot');
    src: url('../assets/fonts/eot/dana-demibold.eot?#iefix') format('embedded-opentype'),  /* IE6-8 */
      url('../assets/fonts/woff2/dana-demibold.woff2') format('woff2'),  /* FF39+,Chrome36+, Opera24+*/
      url('../assets/fonts/woff/dana-demibold.woff') format('woff');  /* FF3.6+, IE9, Chrome6+, Saf5.1+*/
}
@font-face {
  font-family: Dana;
  font-style: normal;
  font-weight: 600;
  src: url('../assets/fonts/eot/dana-ultrabold.eot');
  src: url('../assets/fonts/eot/dana-ultrabold.eot?#iefix') format('embedded-opentype'),  /* IE6-8 */
    url('../assets/fonts/woff2/dana-ultrabold.woff2') format('woff2'),  /* FF39+,Chrome36+, Opera24+*/
    url('../assets/fonts/woff/dana-ultrabold.woff') format('woff');  /* FF3.6+, IE9, Chrome6+, Saf5.1+*/
}

@font-face {
  font-family: Dana;
  font-style: normal;
  font-weight: bold;
  src: url('../assets/fonts/eot/dana-bold.eot');
  src: url('../assets/fonts/eot/dana-bold.eot?#iefix') format('embedded-opentype'),  /* IE6-8 */
    url('../assets/fonts/woff2/dana-bold.woff2') format('woff2'),  /* FF39+,Chrome36+, Opera24+*/
    url('../assets/fonts/woff/dana-bold.woff') format('woff');  /* FF3.6+, IE9, Chrome6+, Saf5.1+*/
}
  @font-face {
    font-family: Dana;
    font-style: normal;
    font-weight: normal;
    src: url('../assets/fonts/eot/dana-regular.eot');
    src: url('../assets/fonts/eot/dana-regular.eot?#iefix') format('embedded-opentype'),  /* IE6-8 */
    url('../assets/fonts/woff2/dana-regular.woff2') format('woff2'),  /* FF39+,Chrome36+, Opera24+*/
    url('../assets/fonts/woff/dana-regular.woff') format('woff');  /* FF3.6+, IE9, Chrome6+, Saf5.1+*/
}
  html {
    font-family: Dana, tahoma, arial;
    color: var(--black);
  }

 

  a {
    color: var(--blue1);

  }

  .center {
    text-align: center;
  }


`;

export default Typography;
