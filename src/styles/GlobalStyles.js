import { createGlobalStyle } from 'styled-components';

const GlobalStyles = createGlobalStyle`
  :root {
    --black: #1c1f26;
    --grey1:#373b42;
    --grey2:#3F434B;
    --grey3:#646A73;
    --grey4:#778193;
    --grey5:#9FA8B8;
    --grey6:#E3E5E8;
    --grey7:#F3F4F7;
    --grey8:#8A8C91;
    --blue1:#3081E9;
    --blue2:#93B9FF;
    --red: #F64F64;
    --orange: #FF5328;
    --pink: #FDDBE0;
    --green: #27BE56;
  }
  html {
    font-size: 16px;

    @media only screen and (max-width: 500px) {
      font-size: 14px;
    }

    @media only screen and (min-width: 2201px) {
      font-size: 18px;
    }
  }

  body {
    font-family: Dana, tahoma, arial;
    font-weight: normal;
    font-size: 1rem;
    line-height: 1.7;
  }

  .container {
    position: relative;
    padding-right: 15px;
    padding-left: 15px;
    margin: 0 auto;
    width: 100%;
 
    @media(min-width:576px){
      max-width:540px;
    }
    @media(min-width:768px){
      max-width:720px;
    }
      
    @media(min-width:992px){
      max-width:960px;
    }
        
    @media(min-width:1200px){
      max-width:1140px;
    }  
  }
  .gatsby-image-wrapper img[src*=base64\\,] {
    image-rendering: -moz-crisp-edges;
    image-rendering: pixelated;
  }

 img {
    max-width: 100%;
  }


`;

export default GlobalStyles;
